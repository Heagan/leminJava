## Lemin
# Pathfinding Project made in Java

Find the best path through a map to reach another point on the map avoiding the walls

I didn't use the A-star algorithm, instead this example solves the map differently.

It recursivly iterates through the map <b>starting from the end point</b> and calculating a "weight" value,
This value indicates how hard it is to get to the end from this position

For each node it will set the weight to n + 1, then go to each neighbouring node and set that weight to n + 1
Example were 'E' is the end and each number is represents its weight:

```
5 4 3 2 3 4 5
4 3 2 1 2 3 4
3 2 1 E 1 2 3
4 3 2 1 2 3 4
5 4 3 2 3 4 5
```

I did this way for fun and this methods' benifit is that once you calculate the weights, no matter where your starting position is you'll instantly know which neighbouring node will be the quickest to reach the end


# To Run

./java Main <mapfile>

The path will then be printed to the terminal and a file (CompleteMap.txt)

<mapfile> needs to follow the example map below

# Problem

Given ANY map, find the shortest route from ‘S’ to ‘E’ avoiding the walls ‘W’, the route taken must be indicated by ‘*’

The map will be a text file and your output can be displayed in the terminal

# Example map
<br>
S - Start
<br>
E - End
<br>
W - Wall
<br>
. - Empty Space
<br>
* - Path

## Unsolved
![Unsolved](https://gitlab.com/Heagan/leminJava/raw/master/unsolved.PNG "Example")

## Solved
![Solved](https://gitlab.com/Heagan/leminJava/raw/master/solved.PNG "Example")


