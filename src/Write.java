import java.io.*;

public class Write {
    public static void writeToMap() {
        String fileName = "CompletedMap.txt";
        int x;
        int y;

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            y = 0;
            while (Main.Map[y][0] != -1)
            {
                x = 0;
                while (Main.Map[y][x] != -1)
                {
                    if (Main.Map[y][x] == 0)
                        bufferedWriter.write("W");
                    if (Main.Map[y][x] == 1)
                        bufferedWriter.write(".");
                    if (Main.Map[y][x] == 2)
                        bufferedWriter.write("S");
                    if (Main.Map[y][x] == 3)
                        bufferedWriter.write("E");
                    if (Main.Map[y][x] == 4)
                        bufferedWriter.write("*");
                    if (Main.Map[y][x] == 5)
                        bufferedWriter.write("\"");
                    bufferedWriter.write(" ");
                    x++;
                }
                bufferedWriter.newLine();
                y++;
            }
            // Always close files.
            bufferedWriter.close();
        } catch (IOException ex) {
            System.out.println("Error writing to file '" + fileName + "'");
        }
    }
}
