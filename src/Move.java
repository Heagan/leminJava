public class Move {

    public static void		changeVal(Room h, Room r)
    {
        if (h.type != 2) // if its not the end
            h.moveb = 1; // we say dont move anything here for this turn !!! I think this should be 'r' since we taking away
        if (h.type == 2)
            h.pop++;
        else
            h.pop = r.pop;
        // we dont dec 'r' here because we still need its pop elsewhere to get the name
    }

    public static void	 	resetMoveb()
    {
        Room    h;

        h = Main.head;
        while (h != null)
        {
            h.moveb = 0;
            h = h.next;
        }
    }

    public static int			movePos()
    {
        Room h;

        h = Main.head;
        while (h != null)
        {
            if (h.pop > 0)
                if (h.moveb == 1)
                    return (1);
            h = h.next;
        }
        return (0);
    }

    public static String        iterateMove(Room r)
    {
        String  slt[];
        Room    tmp;
        int		n;
        int		b;

        slt = r.links.split(" "); // What am I connected to?
        n = slt.length; // save amount of connections
        tmp = Main.head;
        while (tmp != null)
        {
            b = n;
            while (b != 0)
            {
                if (tmp.id.contentEquals(slt[b - 1]) ) // we not moving it to ourself
                    if (tmp.weight != -1 && tmp.moveb != 1) // is it connected to the end? have I already moved something this turn?
                        if ((tmp.pop <= 0 && tmp.weight < r.weight) || tmp.type == 2) // is it empty, is it closer, is it the end?
                        {
                            changeVal(tmp, r);
                            if (tmp.type == 2)
                                return ("END");
                            return (tmp.id);
                        }
                        b--;
            }
            tmp = tmp.next;
        }
        return (null);
    }

    public static void		moveStartAnts(Room r, int ants)
    {
        String  dst;
        String  name;

        while (r.type == 1)
        {
            if (r.pop > 0 && r.moveb == 0)
            {
                if ((dst = iterateMove(r)) != null)
                {
                    name = String.valueOf(ants - r.pop + 1);
                    Set.SetMapIndexStr(dst, 4);
                    System.out.print("L[" + name + ":" + r.id + "->" + dst + "] ");
                    if (r.type == 1)
                        r.pop--;
                    else
                        r.pop = 0;
                }
                else
                    return ;
            }
            else
                return ;
        }
        if (r.type == 1)
            System.out.println("\n");
    }

    public static void		moveProcessResult(Room r, int ants)
    {
        String      dst;
        String      name;

        moveStartAnts(r, ants);
        if (r.pop > 0 && r.moveb == 0)
            if ((dst = iterateMove(r)) != null)
            {
                name = String.valueOf(ants - r.pop + 1);
                Set.SetMapIndexStr(dst, 4);
                System.out.print("L[" + name + ":" + r.id + "->" + dst + "] ");
                if (r.type == 1)
                    r.pop--;
                else
                    r.pop = 0;            }
    }

    public static void		moveAnts(int ants)
    {
        Room tmp;

        tmp = Main.head;
        tmp.moveb = 1;
        while (movePos() == 1)
        {
            resetMoveb();
            tmp = Main.head;
            while (tmp != null)
            {
                moveProcessResult(tmp, ants);
                tmp = tmp.next;
            }
            System.out.println("");
        }
        if (Checks.populated() > 0)
            moveAnts(ants);
    }
}
