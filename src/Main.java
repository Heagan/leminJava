/*
    Map Key
    0 - w
    1 - .
    2 - s
    3 - e
    4 - *
    5 - "
 */
public class Main {
    static Room head = null;
    static int  Map[][] = new int [1000][1000];

    public static void main ( String[] argv )
    {
        if (argv.length == 1)
            Read.openFile(argv[0]);
        else
            Main.Error("No map file specified.\nEnter a file location\neg. java Main ./maps/map");
            //Read.openFile("C:/Users/a233430/IdeaProjects/lem-in/src/maps/map");
        Store.readMap();
        Checks.checkValid();
        Weight.calcWeight();
        Set.SetStartPop(1);
        System.out.println("");
        Move.moveAnts(1);
        Read.closeFile();
        Write.writeToMap();
        System.out.println("");
        printMap();

    }

    public static void Error( String message ) {
        System.out.println( message );
        System.exit(-1);
    }

    public static void printMap() {
        String  line;

        Read.openFile("CompletedMap.txt");
        while ((line = Read.readLn()) != null)
            System.out.println(line);
        Read.closeFile();
        System.out.println("\nCompleted Successfully!");
    }

}
